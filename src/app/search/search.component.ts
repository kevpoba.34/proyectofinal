import { Component, OnInit } from '@angular/core';
import { map, finalize } from 'rxjs/internal/operators';
import { AppRestSService } from '../app/app-rest-s.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  public isLoading = false;
  public src: string | undefined;
  public data$: any;
  constructor(private apiRest: AppRestSService ) { }

  ngOnInit(): void {
  }
  
  search(value: any): any {
    this.isLoading = true;

    this.data$ = this.apiRest.searchTrack({q: value})
      .pipe(
        map(({tracks}) => tracks.items),
        finalize(() => this.isLoading = false)
      )
  }
}
