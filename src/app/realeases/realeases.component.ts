import { Component, OnInit } from '@angular/core';
import { AppRestSService } from '../app/app-rest-s.service';

@Component({
  selector: 'app-realeases',
  templateUrl: './realeases.component.html',
  styleUrls: ['./realeases.component.css']
})
export class RealeasesComponent implements OnInit {
  nuevascanciones: any []=[]; 
  
  constructor(private spotify: AppRestSService) { 
    this.spotify.getNewReleases()
    .subscribe( (data: any) => {
      //console.log( data.albums.items );
      this.nuevascanciones= data;
    });
  }

  ngOnInit(): void {
  }

 

}
