import { Component, Input, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import firebase from 'firebase/app';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

@Input() action:string | undefined;

email='';
pass= '';
  constructor(public auth: AngularFireAuth,
    private router: Router) { }

  ngOnInit(): void {
    console.log(this.action);
  }

  logincongoogle(){
    this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider);
  }

  logout(){
    this.auth.signOut();
  }

  customLogin(){
    this.auth.signInWithEmailAndPassword(this.email,this.pass)
    .then(res =>{
      console.log(res)
    })
    .catch(err => console.log('Eror cl: ', err));
  }

  register(){
    this.auth.createUserWithEmailAndPassword(this.email, this.pass)
    .then( user => {
      console.log(user);
      this.router.navigate(['profile']);
    })
    .catch(err => console.log('Error user:', err) );
  }

  

}
