import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//Lo que fui agragando
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import {AngularFireAuthModule} from '@angular/fire/auth';
import { FormsModule} from '@angular/forms';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { FormComponent } from './components/form/form.component';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { HttpClientModule} from '@angular/common/http';
import { SearchComponent } from './search/search.component';
import { RealeasesComponent } from './realeases/realeases.component';
import { HomeComponent } from './pages/home/home.component';
import { NoimagePipe } from './app/noimage.pipe';
import { PlaylistComponent } from './pages/playlist/playlist.component';

//New

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    FormComponent,
    SearchComponent,
    RealeasesComponent,
    HomeComponent,
    NoimagePipe,
    PlaylistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    FormsModule,
    AngularFireAuthGuardModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
