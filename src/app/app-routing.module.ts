import { NgModule } from '@angular/core';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RegisterComponent } from './pages/register/register.component';
import { RealeasesComponent } from './realeases/realeases.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent },
  {path: 'newReales', component: RealeasesComponent },
  {path: 'searcha', component: HomeComponent , canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent , canActivate: [AuthGuard]},
  {path: 'search', component: SearchComponent , canActivate: [AuthGuard]},
  {path: '**', pathMatch: 'full', redirectTo: 'login'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
