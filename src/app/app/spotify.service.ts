import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  getQuety( query: string){

    const url = `https://api.spotify.com/v1/${ query}`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQAASeq5Nf7LpSrXfVJQjh9OO_qFqHcWA2HsWWQns3PWXlCRXpKc4Pj0tIq-pKuUlReSwubYlCCzXBmYqlE'
    });

    return this.httpClient.get(url, {headers});
  }
    

   
    
  constructor(private httpClient: HttpClient) { }

  getArtista(termino: string){
    
    return this.getQuety(`search?q=${ termino }&type=artist`)
    .pipe(map((data: any) => data.artists.items));
     
     
  }
  
}
