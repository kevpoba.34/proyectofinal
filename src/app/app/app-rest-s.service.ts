import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { map } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class AppRestSService {
  private headerCustom: HttpHeaders | undefined;
   

  url= 'https://api.spotify.com/v1/search'
  token ='BQByfdTxesmUauNphwz1mDGocXQJxg98_G_WsgTRC3eeUinX6nR4hRsJrFx4HdfrP1vVaJYHpKgqGJuTGXs';
  url2='https://api.spotify.com/v1/browse/new-releases?limit=20';
  constructor(private httpClient: HttpClient) {
    
    
   }

   getQuety( query: string){

    const url = `https://api.spotify.com/v1/${ query}`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQByfdTxesmUauNphwz1mDGocXQJxg98_G_WsgTRC3eeUinX6nR4hRsJrFx4HdfrP1vVaJYHpKgqGJuTGXs'
    });

    return this.httpClient.get(url, {headers});
  }



   searchTrack({q}: TrackModel): Observable<any> {

    this.headerCustom = new HttpHeaders({
      Authorization: `Bearer ${this.token}`
    });

    return this.httpClient.get(`${this.url}?q=${q}&type=track&limit=10`,
      {headers: this.headerCustom});
  }


  
  getNewReleases(){
    return this.getQuety('browse/new-releases?limit=20')
     .pipe( map( (data:any)  =>  data['albums'].items )) 
  }

  getArtista(termino: string){
    return this.getQuety(`search?q=${ termino }&type=artist`)
    .pipe(map((data: any) => data.artists.items));
  }

 getPlayList(){
  return this.getQuety('browse/featured-playlists?country=MX')
   .pipe( map( (data:any)  =>  data['playlists'].items )); 
    }


}

export class TrackModel {
  q: string | undefined;
}
