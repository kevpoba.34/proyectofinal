import { Component, OnInit } from '@angular/core';
import { AppRestSService } from 'src/app/app/app-rest-s.service';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {
  nuevascanciones: any []=[]; 
  constructor(private spotify: AppRestSService) {
    
    this.spotify.getPlayList()
    .subscribe( (data: any) => {
      //console.log( data.albums.items );
      this.nuevascanciones= data;
    });
   }

  ngOnInit(): void {
  }

}
