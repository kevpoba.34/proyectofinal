import { Component, OnInit } from '@angular/core';
import { AppRestSService } from 'src/app/app/app-rest-s.service';
import { SpotifyService } from 'src/app/app/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public searchQuery: string="";
  artistas: any[]=[];

  constructor(private spotyservice: AppRestSService) { }

  ngOnInit(): void {
  }

  buscar ( termino: string){
    console.log(termino);

    this.spotyservice.getArtista( termino )
    .subscribe( (data:any) => {
      console.log(data);
      this.artistas = data;
    });
  }

}
