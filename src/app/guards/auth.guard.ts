import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { take, switchMap} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth: AngularFireAuth,
    private router: Router){

  }
  
  canActivate(){

    return this.auth.authState.pipe(
      take(1),
      switchMap(async (authState) => {
          if (authState) {
              return true;
          } else {
              console.log('No autenticado');
              this.router.navigate(['/auth/login'])
              return false
          }
      }),
  )

}

}
