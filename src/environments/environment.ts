// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBfp4AJj9y4MeErg8irMjoZ6f9MRLx2j3A",
    authDomain: "angularp21.firebaseapp.com",
    projectId: "angularp21",
    storageBucket: "angularp21.appspot.com",
    messagingSenderId: "533475399891",
    appId: "1:533475399891:web:6b8f6dca4d2e213b2d1c26",
    measurementId: "G-03TR3NRF5P"
  },
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
